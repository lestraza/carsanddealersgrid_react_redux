export const normalizeCars = (cars, dealers) => {
    return cars.map((car) => {
        let newCar = {
            vin: car.vin,
            brand: car.brand,
            model: car.model,
            grade: car.grade,
            dealersName: '',
            dealersAddress: '',
        }

        dealers.forEach((dealer) => {
            if (car.dealer === dealer.id) {
                const officeId = car.office_ids[0]
                dealer.offices.forEach((office) => {
                    if (officeId === office.id) {
                        newCar.dealersName = dealer.name
                        newCar.dealersAddress = office.address
                    }
                })
            }
        })
        return newCar
    })
}
