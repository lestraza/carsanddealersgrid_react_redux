export const normalizeDealers = (dealers) => {
    return dealers.map((dealer) => {
        let newDealer = {
            id: dealer.id,
            name: dealer.name,
            offices: dealer.offices,
        }
        return newDealer
    })
}
