import React, { Component } from 'react'

export default class TableRow extends Component {
    renderCells = () => {
        const { rowData } = this.props
        return Object.keys(rowData).map(key => {
            return (
                <td className="table__column content" key={key}>{rowData[key]}</td>
            )
        })
    }

    render() {

        return (
            <tr>
                {this.renderCells()}
            </tr>
        )
    }
}
