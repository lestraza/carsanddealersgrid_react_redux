import React, { Component } from 'react'
import { connect } from 'react-redux'
import { loadCars } from '../actions/actions'
import Loader from 'react-loader-spinner'
import Table from './Table'

class App extends Component {
    componentDidMount() {
        this.props.loadCars()
    }

    render() {
        return (
            <div className="container-fluid">
                {this.props.isLoading ? (
                    <div className="spinner">
                        <Loader
                            type="ThreeDots"
                            color="#00BFFF"
                            height={100}
                            width={100}
                        />
                    </div>
                ) : (
                    <Table />
                )}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cars: state.cars,
        dealers: state.dealers,
        isLoading: state.isLoading,
    }
}

const mapDispatchToProps = {
    loadCars,
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
