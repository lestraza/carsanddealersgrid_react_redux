import React, { Component } from 'react'
import { connect } from 'react-redux'
import { loadCars } from '../actions/actions'
import TableRow from './TableRow'
import 'antd/dist/antd.css'
import { Pagination } from 'antd'

const columnTitles = [
    'vin',
    'brand',
    'model',
    'grade',
    'dealer',
    "dealer's adress",
]
class Table extends Component {
    renderRow = () => {
        const { cars } = this.props
        return cars.map((car) => {
            return <TableRow rowData={car} key={car.vin} />
        })
    }

    renderColumnTitles = () => {
        return columnTitles.map((title) => {
            return (
                <th className="table__column title" key={title}>
                    {title.toUpperCase()}
                </th>
            )
        })
    }

    onChangeGetNewPage = (page) => {
        this.props.loadCars(page)
    }

    render() {
        const { totalPages, currentPage } = this.props
        return (
            <>
                <table className="table">
                    <tbody>
                        <tr className="table__row">
                            {this.renderColumnTitles()}
                        </tr>
                        {this.props.cars.length > 0 && this.renderRow()}
                    </tbody>
                </table>
                <Pagination
                    defaultCurrent={currentPage}
                    total={totalPages * 10}
                    showSizeChanger={false}
                    onChange={this.onChangeGetNewPage}
                />
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cars: state.cars,
        currentPage: state.currentPage,
        totalPages: state.totalPages,
        isLoading: state.isLoading,
    }
}
const mapDispatchToProps = {
    loadCars,
}

export default connect(mapStateToProps, mapDispatchToProps)(Table)
