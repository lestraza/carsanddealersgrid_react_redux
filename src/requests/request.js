import axios from 'axios'
import { normalizeDealers } from '../utils/normalizeDealers'

const perPage = 20
function getCarsPerPage(page) {
    axios.defaults.headers.common['X-CS-Dealer-Id-Only'] = '1'
    return axios.get(
        `https://jlrc.dev.perx.ru/carstock/api/v1/vehicles/?state=active&hidden=false&group=new&page=${page}&per_page=${perPage}`,
    )
}
function getDealersById(ids) {
    return axios
        .get(`https://jlrc.dev.perx.ru/carstock/api/v1/dealers/?id__in=${ids}`)
        .then((res) => res.data)
}

export const getCars = async (page) => {
    const resCars = await getCarsPerPage(page)
    const result = {
        totalPages: Math.floor(
            parseInt(resCars.headers['x-total-count'] / perPage),
        ),
        cars: resCars.data,
    }
    return result
}

export const getDealers = async (dealersIds) => {
    const resDealers = await getDealersById(dealersIds)
    const result = normalizeDealers(resDealers)
    return result
}
