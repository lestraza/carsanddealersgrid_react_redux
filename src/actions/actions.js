import { getCars, getDealers } from '../requests/request'
import {
    actionCreatorIsLoading,
    actionCreatorLoadedData,
} from '../actionCreators/actionCreators'
import { normalizeCars } from '../utils/normalizeCars'

export const loadCars = (pageNumber) => {
    return async (dispatch, getState) => {
        const { currentPage, dealers } = getState()
        let page = pageNumber || currentPage
        dispatch(actionCreatorIsLoading())
        const response = await getCars(page)
            const strDealersIds = getDealerIds(response.cars, dealers)
            if (strDealersIds) {
                getDealers(strDealersIds).then((resDealers) => {
                    const normalizedCars = normalizeCars(response.cars, [
                        ...dealers,
                        ...resDealers,
                    ])
                    dispatch(
                        actionCreatorLoadedData(
                            normalizedCars,
                            resDealers,
                            response.totalPages,
                            page,
                        ),
                    )
                })
            } else {
                const normalizedCars = normalizeCars(response.cars, dealers)
                dispatch(
                    actionCreatorLoadedData(
                        normalizedCars,
                        [],
                        response.totalPages,
                        page,
                    ),
                )
            }
    }
}
const getDealerIds = (cars, dealers) => {
    let result
    const carsDealerId = cars.map((car) => car.dealer).filter((id) => id)
    const uniqDeealersId = [...new Set(carsDealerId)]
    if (dealers.length > 0) {
        const dealerId = dealers.map((dealer) => dealer.id)
        const filteredId = uniqDeealersId.filter(
            (id) => dealerId.indexOf(id) === -1,
        )
        result = filteredId.join(',')
    } else {
        result = uniqDeealersId.join(',')
    }
    return result
}
