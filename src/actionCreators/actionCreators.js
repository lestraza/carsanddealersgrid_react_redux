import { IS_LOADING, LOAD_DATA } from '../constants/constants'

export const actionCreatorIsLoading = () => {
    return { type: IS_LOADING }
}
export const actionCreatorLoadedData = (
    cars,
    dealers,
    totalPages,
    pageNumber,
) => {
    return {
        type: LOAD_DATA,
        payload: { cars, dealers, totalPages, pageNumber },
    }
}
