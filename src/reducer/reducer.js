import { IS_LOADING, LOAD_DATA } from '../constants/constants'

const initialState = {
    cars: [],
    dealers: [],
    isLoading: false,
    currentPage: 1,
    totalPages: 0,
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case IS_LOADING:
            return {
                ...state,
                isLoading: true,
            }
        case LOAD_DATA:
            return {
                ...state,
                isLoading: false,
                cars: [...action.payload.cars],
                dealers: [...state.dealers, ...action.payload.dealers],
                currentPage: action.payload.pageNumber,
                totalPages: action.payload.totalPages,
            }
        default:
            return state
    }
}
export default reducer
